import random as rado
def create_grid(size): #创建size*size空列表并全为‘’
    game_grid = []
    for i in range(0,size):
        game_grid.append ([])
        for j in range(0,size):
            game_grid[i].append(' ')
    return game_grid

def grid_add_new_tile_at_position(game_grid,x,y):#将（x,y)位置添加新数字
    game_grid[x][y]=get_value_new_tile()
    return game_grid

def get_value_new_tile():#创建随机数2或4
    a=rado.random()
    if a<0.9:
        return 2
    else:
        return 4

def get_all_tiles(game_grid):#将所有位置的数字放入一维list
    all_tiles=[]
    size=len(game_grid)
    for i in range(size):
        for j in range(size):
            if game_grid[i][j]==' ':
                all_tiles.append(0)
            else:
                all_tiles.append(game_grid[i][j])
    return all_tiles

def get_empty_tiles_positions(game_grid):#搜寻列表中所有为空或0的坐标
    size=len(game_grid)
    empty_title_positon=[]
    for i in range(size):
        for j in range(size):
            if game_grid[i][j] in [' ',0]:
                empty_title_positon.append((i,j))
    return empty_title_positon

def get_new_position(game_grid): #随机返回一组空白坐标
    empty_tiles=get_empty_tiles_positions(game_grid)
    (x,y)=rado.choice(empty_tiles)
    return x,y

def grid_get_value(grid,x,y):#返回某一点值
    if grid[x][y]==' ':
        return 0
    else:
        return grid[x][y]

def grid_add_new_tile(game_grid):#在随机空白位置添加新数
    x,y=get_new_position(game_grid)
    game_grid=grid_add_new_tile_at_position(game_grid,x,y)
    return game_grid

def init_game(size):#初始化生成两个随机数
    game_grid=create_grid(size)
    game_grid=grid_add_new_tile(game_grid)
    game_grid=grid_add_new_tile(game_grid)
    return game_grid
