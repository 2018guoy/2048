def read_player_command():#键入移动方向
    list_move_grid = ['g','d','h','b']
    while True:
        try:
            move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
            move = move.lower()
            if move not in list_move_grid:
                raise ValueError
            else:
                break
        except:
            print('您输入的内容不规范，请重新输入：')
    return move

def read_size_grid():#键入矩阵边长
    list_size_grid=['2','3','4','5','6']
    while True:
        try:
            size_grid = input("Entrez la taille(de 2 a 6)")
            size_grid=size_grid.lower()
            if size_grid not in list_size_grid:
                raise ValueError
            else:
                break
        except:
            print('您输入的内容不规范，请重新输入：')
    return size_grid


def read_theme_grid():#键入主题
    list_theme_grid=['0','1','2']
    while True:
        try:
            theme_grid = input("Entrez le theme('0' pour Default,'1' pour Chemistry,'2' pour Alphabet)")
            if theme_grid not in list_theme_grid:
                raise ValueError
            else:
                break
        except:
            print('您输入的内容不规范，请重新输入：')
    return theme_grid


