import grid_2048 as gr
'''
def grid_to_string(game_grid,size):
    row=' === === === ===\n|'
    num=[]
    a='\n'
    for i in range(size):
        a=a+row
        for j in range(size):
            a=a+' '+str(grid[i][j])+' |'
        a+='\n'
    a=a+row
    a=a[0:-1]
    a+='   '
    print(a)
'''
def long_value(grid):#求图形最长长度
    all_tiles=gr.get_all_tiles(grid)
    max=1
    lenth=len(all_tiles)
    for i in range(lenth):
        all_tiles[i]=str(all_tiles[i])
        if max<len(all_tiles[i]):
            max =len(all_tiles[i])
    return max

def long_value_with_theme(grid,THEMES):
    if THEMES["name"]=='Default':
        min=3
    elif THEMES["name"]=='Chemistry':
        return 2
    else:
        return 1
    Max=long_value(grid)
    return max([Max,min])

def grid_to_string_with_size_and_theme(game_grid,THEMES,size):
    pic_grid=size*[size*[' ']]#n*n的字符串形式的初始化数组
    pic_grid_print=''#即将打印的图形初始化
    longeur=long_value_with_theme(game_grid,THEMES)
    row='='*(size*(longeur+1)+1)+'\n|'#计算出“=”的长度并赋予到row字符串中
    pic_grid_print+=row#将===加入至打印的图形
    for i in range(size):
        for j in range(size):
            a=THEMES.get(game_grid[i][j])#查字典得出某数的theme对应pic
            l=str(a)
            pic_grid[i][j]=l.center(longeur,' ')#得出每个字符长度相等且中心化的数组
            pic_grid_print+=str(pic_grid[i][j])+'|'
        pic_grid_print=pic_grid_print+'\n'+row
        #print(game_grid)
    pic_grid_print=pic_grid_print[0:-1]
    return pic_grid_print
