import grid_2048 as gr
import deplacement_2048 as de
def is_grid_full(grid):
    tiles=gr.get_all_tiles(grid)
    return (0 not in tiles) #True则已满 False则未满

def move_possible(grid):
    move_grid_possible=[]#上下左右移动矩阵的可能性
    move_after=[de.move_grid(grid,'up'),de.move_grid(grid,'down'),de.move_grid(grid,'left'),de.move_grid(grid,'right')]
    #print(move_after)
    for i in range(len(move_after)):#i为某一方向移动后矩阵的下标
        for j in range(len(move_after[i])):#j为某一行的下标
            if move_after[i][j]!=grid[j]:
                move_grid_possible.append(True)
                break
        if len(move_grid_possible)!=(i+1):
            move_grid_possible.append(False)
    return move_grid_possible

def is_game_over(grid):
    return move_possible(grid) == [False]*4

def get_grid_tile_max(grid):
    return max(gr.get_all_tiles(grid))

def win_or_not(grid):
    return get_grid_tile_max(grid)>=2048

