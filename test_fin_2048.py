import fin_2048 as fi
def test_is_grid_full():
    assert fi.is_grid_full([[2,0,0,2], [4, 4, 0, 0], [8, 0, 8, 0], [0, 2, 2, 0]])==False
    assert fi.is_grid_full([[2,2,2,2], [4, 4, 2, 2], [8, 2, 8, 2], [2, 2, 2, 2]])==True

def test_move_possible():
    assert fi.move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == [True,True,True,True]
    assert fi.move_possible([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == [False,False,False,False]

def test_is_game_over():
    assert fi.is_game_over([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == False
    assert fi.is_game_over([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == True
    assert fi.is_game_over([[2, 4, 8], [16, 8, 4], [2, 4, 8]]) == True
    assert fi.is_game_over([[2, 2, 2], [4, 8, 8], [0, 8, 0]]) == False

def test_get_grid_tile_max():
    assert fi.get_grid_tile_max([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]])==32
    assert fi.get_grid_tile_max([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 64]])==64
    assert fi.get_grid_tile_max([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 128, 32]])==128
    assert fi.get_grid_tile_max([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 2048, 32]])==2048

def test_win_or_not():
    assert fi.win_or_not([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 2048, 32]])==True
    assert fi.win_or_not([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 4096, 32]])==True
    assert fi.win_or_not([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 1024, 32]])==False
