def move_row_left(row_list):#左靠一行
    length=len(row_list)
    rid_list=[x for x in row_list if (x!=0 and x!=' ')]
    i=0
    while i<(len(rid_list)-1):
        if rid_list[i]==rid_list[i+1]:
            rid_list[i]*=2
            del rid_list[i+1]
        i+=1
    for j in range(length-len(rid_list)):
        rid_list.append(0)
    return rid_list

def move_row_right(row_list):#右靠一行
    row_list=row_list[::-1]
    row_list=move_row_left(row_list)
    row_list=row_list[::-1]
    return row_list

def transform(m):#转置矩阵
    a = [[] for i in m[0]]
    for i in m:
        for j in range(len(i)):
            a[j].append(i[j])
    return a

def move_grid(grid,d):#向d方向移动整个矩阵
    if d == 'up' or d == 'down':
        grid=transform(grid)
    move_row_grid=[]
    for i in range(len(grid)):
        if d == 'left':
            move_row_grid.append(move_row_left(grid[i]))
        elif d == 'right':
            move_row_grid.append(move_row_right(grid[i]))
        elif d == 'up':
            move_row_grid.append(move_row_left(grid[i]))
        elif d == 'down':
            move_row_grid.append(move_row_right(grid[i]))
    if d == 'up' or d == 'down':
        move_row_grid=transform(move_row_grid)
    return move_row_grid

